SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS OMOP4;
CREATE SCHEMA OMOP4;
USE OMOP4;

CREATE TABLE CONCEPT (
  CONCEPT_ID		INTEGER		NOT NULL,
  CONCEPT_NAME		VARCHAR(256)	NOT NULL,
  CONCEPT_LEVEL		INTEGER		NOT NULL,
  CONCEPT_CLASS		VARCHAR(60)	NOT NULL,
  VOCABULARY_ID		INTEGER		NOT NULL,
  CONCEPT_CODE		VARCHAR(40)	NOT NULL,
  VALID_START_DATE	DATE		NOT NULL,
  VALID_END_DATE	DATE		NOT NULL DEFAULT '2099-12-31',
  INVALID_REASON	CHAR(1)		NULL)

;

ALTER TABLE CONCEPT ADD CONSTRAINT XPKCONCEPT PRIMARY KEY (CONCEPT_ID)


;
ALTER TABLE CONCEPT ADD CHECK ( invalid_reason IN ('D', 'U'))
;
ALTER TABLE CONCEPT ADD CONSTRAINT CONCEPT_VOCABULARY_REF_FK FOREIGN KEY (VOCABULARY_ID) REFERENCES VOCABULARY (VOCABULARY_ID)
;

CREATE TABLE CONCEPT_ANCESTOR (
  ANCESTOR_CONCEPT_ID		INTEGER	NOT NULL,
  DESCENDANT_CONCEPT_ID		INTEGER	NOT NULL,
  MAX_LEVELS_OF_SEPARATION	INTEGER	NULL,
  MIN_LEVELS_OF_SEPARATION	INTEGER	NULL)

;


ALTER TABLE CONCEPT_ANCESTOR ADD CONSTRAINT XPKCONCEPT_ANCESTOR PRIMARY KEY (ANCESTOR_CONCEPT_ID,DESCENDANT_CONCEPT_ID)


;
ALTER TABLE CONCEPT_ANCESTOR ADD CONSTRAINT CONCEPT_ANCESTOR_FK FOREIGN KEY (ANCESTOR_CONCEPT_ID) REFERENCES CONCEPT (CONCEPT_ID)
;
ALTER TABLE CONCEPT_ANCESTOR ADD CONSTRAINT CONCEPT_DESCENDANT_FK FOREIGN KEY (DESCENDANT_CONCEPT_ID) REFERENCES CONCEPT (CONCEPT_ID)
;

CREATE TABLE CONCEPT_RELATIONSHIP (
  CONCEPT_ID_1		INTEGER	NOT NULL,
  CONCEPT_ID_2		INTEGER	NOT NULL,
  RELATIONSHIP_ID	INTEGER	NOT NULL,
  VALID_START_DATE	DATE	NOT NULL,
  VALID_END_DATE	DATE	NOT NULL DEFAULT '2099-12-31',
  INVALID_REASON	CHAR(1)	NULL)

;




ALTER TABLE CONCEPT_RELATIONSHIP ADD CONSTRAINT XPKCONCEPT_RELATIONSHIP PRIMARY KEY (CONCEPT_ID_1,CONCEPT_ID_2,RELATIONSHIP_ID)


;
ALTER TABLE CONCEPT_RELATIONSHIP ADD CHECK ( invalid_reason IN ('D', 'U'))
;
ALTER TABLE CONCEPT_RELATIONSHIP ADD CONSTRAINT CONCEPT_REL_CHILD_FK FOREIGN KEY (CONCEPT_ID_2) REFERENCES CONCEPT (CONCEPT_ID)
;
ALTER TABLE CONCEPT_RELATIONSHIP ADD CONSTRAINT CONCEPT_REL_PARENT_FK FOREIGN KEY (CONCEPT_ID_1) REFERENCES CONCEPT (CONCEPT_ID)
;
ALTER TABLE CONCEPT_RELATIONSHIP ADD CONSTRAINT CONCEPT_REL_REL_TYPE_FK FOREIGN KEY (RELATIONSHIP_ID) REFERENCES RELATIONSHIP (RELATIONSHIP_ID)
;

CREATE TABLE CONCEPT_SYNONYM (
  CONCEPT_SYNONYM_ID	INTEGER		NOT NULL,
  CONCEPT_ID		INTEGER		NOT NULL,
  CONCEPT_SYNONYM_NAME	VARCHAR(1000)	NOT NULL)

;


ALTER TABLE CONCEPT_SYNONYM ADD CONSTRAINT XPKCONCEPT_SYNONYM PRIMARY KEY (CONCEPT_SYNONYM_ID)


;
ALTER TABLE CONCEPT_SYNONYM ADD CONSTRAINT CONCEPT_SYNONYM_CONCEPT_FK FOREIGN KEY (CONCEPT_ID) REFERENCES CONCEPT (CONCEPT_ID)
;

CREATE TABLE DRUG_APPROVAL (
  INGREDIENT_CONCEPT_ID	INTEGER		NOT NULL,
  APPROVAL_DATE		DATE		NOT NULL,
  APPROVED_BY		VARCHAR(20)	NOT NULL DEFAULT 'FDA')

;


CREATE TABLE DRUG_STRENGTH (
  DRUG_CONCEPT_ID		INTEGER		NOT NULL,
  INGREDIENT_CONCEPT_ID		INTEGER		NOT NULL,
  AMOUNT_VALUE			INTEGER		NULL,
  AMOUNT_UNIT			VARCHAR(60)	NULL,
  CONCENTRATION_VALUE		INTEGER		NULL,
  CONCENTRATION_ENUM_UNIT	VARCHAR(60)	NULL,
  CONCENTRATION_DENOM_UNIT	VARCHAR(60)	NULL,
  VALID_START_DATE		DATE		NOT NULL,
  VALID_END_DATE		DATE		NOT NULL,
  INVALID_REASON		VARCHAR(1)	NULL)

;

CREATE TABLE RELATIONSHIP (
  RELATIONSHIP_ID	INTEGER		NOT NULL,
  RELATIONSHIP_NAME	VARCHAR(256)	NOT NULL,
  IS_HIERARCHICAL	INTEGER		NOT NULL,
  DEFINES_ANCESTRY	INTEGER		NOT NULL,
  REVERSE_RELATIONSHIP	INTEGER		NULL)

;


ALTER TABLE RELATIONSHIP ADD CONSTRAINT XPKRELATIONSHIP_TYPE PRIMARY KEY (RELATIONSHIP_ID)


;

CREATE TABLE SOURCE_TO_CONCEPT_MAP (
  SOURCE_CODE			VARCHAR(40)	NOT NULL,
  SOURCE_VOCABULARY_ID		INTEGER		NOT NULL,
  SOURCE_CODE_DESCRIPTION	VARCHAR(256)	NULL,
  TARGET_CONCEPT_ID		INTEGER		NOT NULL,
  TARGET_VOCABULARY_ID		INTEGER		NOT NULL,
  MAPPING_TYPE			VARCHAR(20)	NULL,
  PRIMARY_MAP			CHAR(1)		NULL,
  VALID_START_DATE		DATE		NOT NULL,
  VALID_END_DATE		DATE		NOT NULL DEFAULT '2099-12-31',
  INVALID_REASON		CHAR(1)		NULL)

;




CREATE INDEX SOURCE_TO_CONCEPT_SOURCE_IDX ON SOURCE_TO_CONCEPT_MAP (SOURCE_CODE ASC)

;
ALTER TABLE SOURCE_TO_CONCEPT_MAP ADD CONSTRAINT XPKSOURCE_TO_CONCEPT_MAP PRIMARY KEY (SOURCE_VOCABULARY_ID,TARGET_CONCEPT_ID,SOURCE_CODE,VALID_END_DATE)


;
ALTER TABLE SOURCE_TO_CONCEPT_MAP ADD CHECK ( primary_map IN ('Y'))
;
ALTER TABLE SOURCE_TO_CONCEPT_MAP ADD CHECK ( invalid_reason IN ('D', 'U'))
;
ALTER TABLE SOURCE_TO_CONCEPT_MAP ADD CONSTRAINT SOURCE_TO_CONCEPT_CONCEPT FOREIGN KEY (TARGET_CONCEPT_ID) REFERENCES CONCEPT (CONCEPT_ID)
;
ALTER TABLE SOURCE_TO_CONCEPT_MAP ADD CONSTRAINT SOURCE_TO_CONCEPT_SOURCE_VOCAB FOREIGN KEY (SOURCE_VOCABULARY_ID) REFERENCES VOCABULARY (VOCABULARY_ID)
;
ALTER TABLE SOURCE_TO_CONCEPT_MAP ADD CONSTRAINT SOURCE_TO_CONCEPT_TARGET_VOCAB FOREIGN KEY (TARGET_VOCABULARY_ID) REFERENCES VOCABULARY (VOCABULARY_ID)
;

CREATE TABLE VOCABULARY (
  VOCABULARY_ID		INTEGER		NOT NULL,
  VOCABULARY_NAME	VARCHAR(256)	NOT NULL)

;


ALTER TABLE VOCABULARY ADD CONSTRAINT UNIQUE_VOCABULARY_NAME UNIQUE (VOCABULARY_NAME)


;
ALTER TABLE VOCABULARY ADD CONSTRAINT XPKVOCABULARY_REF PRIMARY KEY (VOCABULARY_ID)


;




CREATE TABLE care_site 
    ( 
     care_site_id INTEGER  NOT NULL , 
     location_id INTEGER , 
     organization_id INTEGER , 
     place_of_service_concept_id INTEGER , 
     care_site_source_value VARCHAR (50) , 
     place_of_service_source_value VARCHAR (50) 
    ) 
;



ALTER TABLE care_site 
    ADD CONSTRAINT care_site_PK PRIMARY KEY ( care_site_id ) ;



CREATE TABLE cohort 
    ( 
     cohort_id INTEGER  NOT NULL , 
     cohort_concept_id INTEGER  NOT NULL , 
     cohort_start_date DATE  NOT NULL , 
     cohort_end_date DATE , 
     subject_id INTEGER  NOT NULL , 
     stop_reason VARCHAR (20) 
    ) 
;




ALTER TABLE cohort 
    ADD CONSTRAINT cohort_PK PRIMARY KEY ( cohort_id ) ;



CREATE TABLE condition_era 
    ( 
     condition_era_id INTEGER  NOT NULL , 
     person_id INTEGER  NOT NULL , 
     condition_concept_id INTEGER  NOT NULL , 
     condition_era_start_date DATE  NOT NULL , 
     condition_era_end_date DATE  NOT NULL , 
     condition_type_concept_id INTEGER  NOT NULL , 
     condition_occurrence_count INTEGER 
    ) 
;




ALTER TABLE condition_era 
    ADD CONSTRAINT condition_era_PK PRIMARY KEY ( condition_era_id ) ;



CREATE TABLE condition_occurrence 
    ( 
     condition_occurrence_id INTEGER  NOT NULL , 
     person_id INTEGER  NOT NULL , 
     condition_concept_id INTEGER  NOT NULL , 
     condition_start_date DATE  NOT NULL , 
     condition_end_date DATE , 
     condition_type_concept_id INTEGER  NOT NULL , 
     stop_reason VARCHAR (20) , 
     associated_provider_id INTEGER , 
     visit_occurrence_id INTEGER , 
     condition_source_value VARCHAR (50) 
    ) 
;




ALTER TABLE condition_occurrence 
    ADD CONSTRAINT condition_occurrence_PK PRIMARY KEY ( condition_occurrence_id ) ;



CREATE TABLE death 
    ( 
     person_id INTEGER  NOT NULL , 
     death_date DATE  NOT NULL , 
     death_type_concept_id INTEGER  NOT NULL , 
     cause_of_death_concept_id INTEGER , 
     cause_of_death_source_value VARCHAR (50) 
    ) 
;




ALTER TABLE death 
    ADD CONSTRAINT death_PK PRIMARY KEY ( person_id, death_type_concept_id ) ;



CREATE TABLE drug_cost 
    ( 
     drug_cost_id INTEGER  NOT NULL , 
     drug_exposure_id INTEGER  NOT NULL , 
     paid_copay INTEGER, 
     paid_coinsurance INTEGER , 
     paid_toward_deductible INTEGER , 
     paid_by_payer INTEGER , 
     paid_by_coordination_benefits INTEGER , 
     total_out_of_pocket INTEGER , 
     total_paid INTEGER , 
     ingredient_cost INTEGER , 
     dispensing_fee INTEGER , 
     average_wholesale_price INTEGER , 
     payer_plan_period_id INTEGER 
    ) 
;



ALTER TABLE drug_cost 
    ADD CONSTRAINT drug_cost_PK PRIMARY KEY ( drug_cost_id ) ;



CREATE TABLE drug_era 
    ( 
     drug_era_id INTEGER  NOT NULL , 
     person_id INTEGER  NOT NULL , 
     drug_concept_id INTEGER  NOT NULL , 
     drug_era_start_date DATE  NOT NULL , 
     drug_era_end_date DATE  NOT NULL , 
     drug_type_concept_id INTEGER  NOT NULL , 
     drug_exposure_count INTEGER 
    ) 
;



ALTER TABLE drug_era 
    ADD CONSTRAINT drug_era_PK PRIMARY KEY ( drug_era_id ) ;



CREATE TABLE drug_exposure 
    ( 
     drug_exposure_id INTEGER  NOT NULL , 
     person_id INTEGER  NOT NULL , 
     drug_concept_id INTEGER  NOT NULL , 
     drug_exposure_start_date DATE  NOT NULL , 
     drug_exposure_end_date DATE , 
     drug_type_concept_id INTEGER  NOT NULL , 
     stop_reason VARCHAR (20) , 
     refills INTEGER , 
     quantity INTEGER , 
     days_supply INTEGER , 
     sig VARCHAR (500) , 
     prescribing_provider_id INTEGER , 
     visit_occurrence_id INTEGER , 
     relevant_condition_concept_id INTEGER , 
     drug_source_value VARCHAR (50) 
    ) 
;




ALTER TABLE drug_exposure 
    ADD CONSTRAINT drug_exposure_PK PRIMARY KEY ( drug_exposure_id ) ;



CREATE TABLE location 
    ( 
     location_id INTEGER  NOT NULL , 
     address_1 VARCHAR (50) , 
     address_2 VARCHAR (50) , 
     city VARCHAR (50) , 
     state CHAR (2) , 
     zip VARCHAR (9) , 
     county VARCHAR (20) , 
     location_source_value VARCHAR (50) 
    ) 
;






ALTER TABLE location 
    ADD CONSTRAINT location_PK PRIMARY KEY ( location_id ) ;



CREATE TABLE observation 
    ( 
     observation_id INTEGER  NOT NULL , 
     person_id INTEGER  NOT NULL , 
     observation_concept_id INTEGER  NOT NULL , 
     observation_date DATE  NOT NULL , 
     observation_time DATE , 
     value_as_number INTEGER , 
     value_as_string VARCHAR (60) , 
     value_as_concept_id INTEGER , 
     unit_concept_id INTEGER , 
     range_low INTEGER , 
     range_high INTEGER , 
     observation_type_concept_id INTEGER  NOT NULL , 
     associated_provider_id INTEGER , 
     visit_occurrence_id INTEGER , 
     relevant_condition_concept_id INTEGER , 
     observation_source_value VARCHAR (50) , 
     units_source_value VARCHAR (50) 
    ) 
;


CREATE INDEX observation_person_idx ON observation 
    ( 
     person_id ASC , 
     observation_concept_id ASC 
    ) 
;

ALTER TABLE observation 
    ADD CONSTRAINT observation_PK PRIMARY KEY ( observation_id ) ;



CREATE TABLE observation_period 
    ( 
     observation_period_id INTEGER  NOT NULL , 
     person_id INTEGER  NOT NULL , 
     observation_period_start_date DATE  NOT NULL , 
     observation_period_end_date DATE  NOT NULL 
    ) 
;



CREATE UNIQUE INDEX observation_period_person ON observation_period 
    ( 
     person_id ASC , 
     observation_period_start_date ASC 
    ) 
;

ALTER TABLE observation_period 
    ADD CONSTRAINT observation_period_PK PRIMARY KEY ( observation_period_id ) ;



CREATE TABLE organization 
    ( 
     organization_id INTEGER  NOT NULL , 
     place_of_service_concept_id INTEGER , 
     location_id INTEGER , 
     organization_source_value VARCHAR (50) , 
     place_of_service_source_value VARCHAR (50) 
    ) 
;



CREATE INDEX organization_oraganization_pos ON organization 
    ( 
     organization_source_value ASC , 
     place_of_service_source_value ASC 
    ) 
;

ALTER TABLE organization 
    ADD CONSTRAINT organization_PK PRIMARY KEY ( organization_id ) ;



CREATE TABLE payer_plan_period 
    ( 
     payer_plan_period_id INTEGER  NOT NULL , 
     person_id INTEGER  NOT NULL , 
     payer_plan_period_start_date DATE  NOT NULL , 
     payer_plan_period_end_date DATE  NOT NULL , 
     payer_source_value VARCHAR (50) , 
     plan_source_value VARCHAR (50) , 
     family_source_value VARCHAR (50) 
    ) 
;




ALTER TABLE payer_plan_period 
    ADD CONSTRAINT payer_plan_period_PK PRIMARY KEY ( payer_plan_period_id ) ;



CREATE TABLE person 
    ( 
     person_id INTEGER  NOT NULL , 
     gender_concept_id INTEGER  NOT NULL , 
     year_of_birth INTEGER  NOT NULL , 
     month_of_birth INTEGER , 
     day_of_birth INTEGER , 
     race_concept_id INTEGER , 
     ethnicity_concept_id INTEGER , 
     location_id INTEGER , 
     provider_id INTEGER , 
     care_site_id INTEGER , 
     person_source_value VARCHAR (50) , 
     gender_source_value VARCHAR (50) , 
     race_source_value VARCHAR (50) , 
     ethnicity_source_value VARCHAR (50) 
    ) 
;




ALTER TABLE person 
    ADD CONSTRAINT PERSON_PK PRIMARY KEY ( person_id ) ;



CREATE TABLE procedure_cost 
    ( 
     procedure_cost_id INTEGER  NOT NULL , 
     procedure_occurrence_id INTEGER  NOT NULL , 
     paid_copay INTEGER , 
     paid_coinsurance INTEGER , 
     paid_toward_deductible INTEGER , 
     paid_by_payer INTEGER , 
     paid_by_coordination_benefits INTEGER , 
     total_out_of_pocket INTEGER , 
     total_paid INTEGER , 
     disease_class_concept_id INTEGER , 
     revenue_code_concept_id INTEGER , 
     payer_plan_period_id INTEGER , 
     disease_class_source_value VARCHAR (50) , 
     revenue_code_source_value VARCHAR (50) 
    ) 
;





ALTER TABLE procedure_cost 
    ADD CONSTRAINT procedure_cost_PK PRIMARY KEY ( procedure_cost_id ) ;



CREATE TABLE procedure_occurrence 
    ( 
     procedure_occurrence_id INTEGER  NOT NULL , 
     person_id INTEGER  NOT NULL , 
     procedure_concept_id INTEGER  NOT NULL , 
     procedure_date DATE  NOT NULL , 
     procedure_type_concept_id INTEGER  NOT NULL , 
     associated_provider_id INTEGER , 
     visit_occurrence_id INTEGER , 
     relevant_condition_concept_id INTEGER , 
     procedure_source_value VARCHAR (50) 
    ) 
;





ALTER TABLE procedure_occurrence 
    ADD CONSTRAINT procedure_occurrence_PK PRIMARY KEY ( procedure_occurrence_id ) ;



CREATE TABLE provider 
    ( 
     provider_id INTEGER  NOT NULL , 
     NPI VARCHAR (20) , 
     DEA VARCHAR (20) , 
     specialty_concept_id INTEGER , 
     care_site_id INTEGER , 
     provider_source_value VARCHAR (50)  NOT NULL , 
     specialty_source_value VARCHAR (50) 
    ) 
;





ALTER TABLE provider 
    ADD CONSTRAINT provider_PK PRIMARY KEY ( provider_id ) ;



CREATE TABLE visit_occurrence 
    ( 
     visit_occurrence_id INTEGER  NOT NULL , 
     person_id INTEGER  NOT NULL , 
     visit_start_date DATE  NOT NULL , 
     visit_end_date DATE  NOT NULL , 
     place_of_service_concept_id INTEGER  NOT NULL , 
     care_site_id INTEGER , 
     place_of_service_source_value VARCHAR (50) 
    ) 
;




CREATE INDEX visit_occurrence_peson_date ON visit_occurrence 
    ( 
     person_id ASC , 
     visit_start_date ASC 
    ) 
;

ALTER TABLE visit_occurrence 
    ADD CONSTRAINT visit_occurrence_PK PRIMARY KEY ( visit_occurrence_id ) ;




ALTER TABLE care_site 
    ADD CONSTRAINT care_site_location_FK FOREIGN KEY 
    ( 
     location_id
    ) 
    REFERENCES location 
    ( 
     location_id
    ) 
;


ALTER TABLE care_site 
    ADD CONSTRAINT care_site_organization_FK FOREIGN KEY 
    ( 
     organization_id
    ) 
    REFERENCES organization 
    ( 
     organization_id
    ) 
;


ALTER TABLE condition_era 
    ADD CONSTRAINT condition_era_PERSON_FK FOREIGN KEY 
    ( 
     person_id
    ) 
    REFERENCES person 
    ( 
     person_id
    ) 
;


ALTER TABLE condition_occurrence 
    ADD CONSTRAINT condition_occurrence_PERSON_FK FOREIGN KEY 
    ( 
     person_id
    ) 
    REFERENCES person 
    ( 
     person_id
    ) 
;


ALTER TABLE condition_occurrence 
    ADD CONSTRAINT condition_provider_FK FOREIGN KEY 
    ( 
     associated_provider_id
    ) 
    REFERENCES provider 
    ( 
     provider_id
    ) 
;


ALTER TABLE condition_occurrence 
    ADD CONSTRAINT condition_visit_FK FOREIGN KEY 
    ( 
     visit_occurrence_id
    ) 
    REFERENCES visit_occurrence 
    ( 
     visit_occurrence_id
    ) 
;


ALTER TABLE death 
    ADD CONSTRAINT death_PERSON_FK FOREIGN KEY 
    ( 
     person_id
    ) 
    REFERENCES person 
    ( 
     person_id
    ) 
;


ALTER TABLE drug_cost 
    ADD CONSTRAINT drug_cost_drug_exposure_FK FOREIGN KEY 
    ( 
     drug_exposure_id
    ) 
    REFERENCES drug_exposure 
    ( 
     drug_exposure_id
    ) 
;


ALTER TABLE drug_cost 
    ADD CONSTRAINT drug_cost_payer_plan_period_FK FOREIGN KEY 
    ( 
     payer_plan_period_id
    ) 
    REFERENCES payer_plan_period 
    ( 
     payer_plan_period_id
    ) 
;


ALTER TABLE drug_era 
    ADD CONSTRAINT drug_era_PERSON_FK FOREIGN KEY 
    ( 
     person_id
    ) 
    REFERENCES person 
    ( 
     person_id
    ) 
;


ALTER TABLE drug_exposure 
    ADD CONSTRAINT drug_exposure_PERSON_FK FOREIGN KEY 
    ( 
     person_id
    ) 
    REFERENCES person 
    ( 
     person_id
    ) 
;


ALTER TABLE drug_exposure 
    ADD CONSTRAINT drug_exposure_provider_FK FOREIGN KEY 
    ( 
     prescribing_provider_id
    ) 
    REFERENCES provider 
    ( 
     provider_id
    ) 
;


ALTER TABLE drug_exposure 
    ADD CONSTRAINT drug_visit_FK FOREIGN KEY 
    ( 
     visit_occurrence_id
    ) 
    REFERENCES visit_occurrence 
    ( 
     visit_occurrence_id
    ) 
;


ALTER TABLE observation 
    ADD CONSTRAINT observation_PERSON_FK FOREIGN KEY 
    ( 
     person_id
    ) 
    REFERENCES person 
    ( 
     person_id
    ) 
;


ALTER TABLE observation_period 
    ADD CONSTRAINT observation_period_PERSON_FK FOREIGN KEY 
    ( 
     person_id
    ) 
    REFERENCES person 
    ( 
     person_id
    ) 
;


ALTER TABLE observation 
    ADD CONSTRAINT observation_provider_FK FOREIGN KEY 
    ( 
     associated_provider_id
    ) 
    REFERENCES provider 
    ( 
     provider_id
    ) 
;


ALTER TABLE observation 
    ADD CONSTRAINT observation_visit_FK FOREIGN KEY 
    ( 
     visit_occurrence_id
    ) 
    REFERENCES visit_occurrence 
    ( 
     visit_occurrence_id
    ) 
;


ALTER TABLE organization 
    ADD CONSTRAINT organization_location_FK FOREIGN KEY 
    ( 
     location_id
    ) 
    REFERENCES location 
    ( 
     location_id
    ) 
;


ALTER TABLE payer_plan_period 
    ADD CONSTRAINT payer_plan_period_PERSON_FK FOREIGN KEY 
    ( 
     person_id
    ) 
    REFERENCES person 
    ( 
     person_id
    ) 
;


ALTER TABLE person 
    ADD CONSTRAINT person_care_site_FK FOREIGN KEY 
    ( 
     care_site_id
    ) 
    REFERENCES care_site 
    ( 
     care_site_id
    ) 
;


ALTER TABLE person 
    ADD CONSTRAINT person_location_FK FOREIGN KEY 
    ( 
     location_id
    ) 
    REFERENCES location 
    ( 
     location_id
    ) 
;


ALTER TABLE person 
    ADD CONSTRAINT person_provider_FK FOREIGN KEY 
    ( 
     provider_id
    ) 
    REFERENCES provider 
    ( 
     provider_id
    ) 
;


ALTER TABLE procedure_cost 
    ADD CONSTRAINT procedure_cost_payer_plan_FK FOREIGN KEY 
    ( 
     payer_plan_period_id
    ) 
    REFERENCES payer_plan_period 
    ( 
     payer_plan_period_id
    ) 
;


ALTER TABLE procedure_cost 
    ADD CONSTRAINT procedure_cost_procedure_FK FOREIGN KEY 
    ( 
     procedure_occurrence_id
    ) 
    REFERENCES procedure_occurrence 
    ( 
     procedure_occurrence_id
    ) 
;


ALTER TABLE procedure_occurrence 
    ADD CONSTRAINT procedure_occurrence_PERSON_FK FOREIGN KEY 
    ( 
     person_id
    ) 
    REFERENCES person 
    ( 
     person_id
    ) 
;


ALTER TABLE procedure_occurrence 
    ADD CONSTRAINT procedure_provider_FK FOREIGN KEY 
    ( 
     associated_provider_id
    ) 
    REFERENCES provider 
    ( 
     provider_id
    ) 
;


ALTER TABLE procedure_occurrence 
    ADD CONSTRAINT procedure_visit_FK FOREIGN KEY 
    ( 
     visit_occurrence_id
    ) 
    REFERENCES visit_occurrence 
    ( 
     visit_occurrence_id
    ) 
;


ALTER TABLE provider 
    ADD CONSTRAINT provider_care_site_FK FOREIGN KEY 
    ( 
     care_site_id
    ) 
    REFERENCES care_site 
    ( 
     care_site_id
    ) 
;


ALTER TABLE visit_occurrence 
    ADD CONSTRAINT visit_occurrence_PERSON_FK FOREIGN KEY 
    ( 
     person_id
    ) 
    REFERENCES person 
    ( 
     person_id
    ) 
;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
